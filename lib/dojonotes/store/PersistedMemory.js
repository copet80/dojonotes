/**
 * Extension of Memory dojo store that is persisted using window.localStorage.
 */
define([
    "dojo/_base/declare",

    // Parent Classes
    "dojo/store/Memory",

    // General Modules
    "dojo/json"

], function(declare, Memory, json) {
    return declare("dojonotes.store.PersistedMemory", [Memory], {
        _idPrefix: "dojonotes_",
        _localStorage: null,

        /**
         * @inheritDoc
         */
        constructor: function(options) {
            // super constructor is called automatically here
            if (!window.localStorage) {
                throw Error("PersistedMemory is not available");
            } else {
                this._localStorage = window.localStorage;
                this.load();
            }
        },

        load: function() {
            this.data = [];
            var len = localStorage.length;
            var i;
            var obj;
            var objId;
            for (i=0; i<len; i++) {
                objId = localStorage.key(i);
                if (objId.indexOf(this._idPrefix) !== 0) {
                    continue;
                }
                objId = objId.replace(this._idPrefix, "");
                obj = this.get(objId);
                if (obj) {
                    this.data.push(obj);
                }
            }
            this.data.sort(function(a, b){ return (a.id * 1) < (b.id * 1) ? -1 : 1; });
        },

        /**
         * @inheritDoc
         */
        get: function(id) {
            var obj = this.inherited(arguments);
            var persistedObj = this._localStorage.getItem(this._idPrefix + id);
            if (persistedObj) {
                obj = json.parse(persistedObj);
            }
            return obj;
        },

        /**
         * @inheritDoc
         */
        put: function(object, options) {
            var id = this.inherited(arguments);
            this._localStorage.setItem(this._idPrefix + id, json.stringify(object));
            return id;
        },

        /**
         * @inheritDoc
         */
        remove: function(id){
            this.inherited(arguments);
            this._localStorage.removeItem(this._idPrefix + id);
        }
    });
});