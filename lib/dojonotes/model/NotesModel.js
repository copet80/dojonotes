/**
 * Manages data for notes app.
 */
define([
    "dojo/_base/declare",
    "dojo/_base/array",
    "dojo/_base/lang",

    // Parent Classes
    "dojo/Evented",

    // General Modules
    //"dojo/store/Memory",
    "dojonotes/store/PersistedMemory",
    "dojo/store/Observable"

], function(declare, array, lang, Evented, Memory, Observable) {
    return declare("dojonotes.model.NotesModel", [Evented], {

        /**
         * Default data, to be replaced from store
         */
        data: {
            id: "dojonotes",
            identifier: "id",
            items: []
        },

        numItems: 0,
        store: null,

        /**
         * @inheritDoc
         */
        constructor: function() {
            var data = /*this.store.get(this.data.id) || */this.data;
            this.store = new Observable(new Memory({data: data}));
            this.invalidateCount();
        },

        /**
         * Invalidates the number of count and perform a recount.
         */
        invalidateCount: function() {
            this.numItems = this.store.query({}).length;
            this.emit("change", this);
        },

        /**
         * Gets one note by ID.
         *
         * @param id Note ID.
         * @returns String.
         */
        getOne: function(id) {
            return this.store.get(id);
        },

        /**
         * Adds one note, ID is automatically generated based on time.
         *
         * @param title Note title.
         * @param content Note content.
         */
        addOne: function(title, content) {
            this.store.put({
                id: Date.now(),
                title: title,
                content: content
            });
            this.invalidateCount();
        },

        /**
         * Removes one note by ID.
         *
         * @param id Note ID.
         */
        removeOne: function(id) {
            this.store.remove(id);
            this.invalidateCount();
        },

        /**
         * Removes multiple using a comma-delimited note IDs.
         *
         * @param ids Note ID strings.
         */
        removeMultiple: function(ids) {
            array.forEach(ids.split(","), lang.hitch(this, this.removeOne));
            this.invalidateCount();
        },

        /**
         * Updates one note by ID.
         *
         * @param id Note ID.
         * @param title Note title.
         * @param content Note content.
         */
        updateOne: function(id, title, content) {
            this.store.put({
                id: id,
                title: title,
                content: content
            });
        }
    });
});