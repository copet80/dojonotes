/**
 * Main application.
 */
define([
    "dojo/_base/declare",
    "dojo/_base/lang",

    // Parent Classes
    "dijit/layout/BorderContainer",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",

    // General Modules
    "dojo/dom",
    "dojo/hash",
    "dojo/topic",
    "dijit/registry",
    "dojo/on",

    // Custom Modules
    "dojonotes/views/NotesList",
    "dojonotes/views/NoteUpdateDialog",
    "dojonotes/model/NotesModel",

    // Widget Template
    "dojo/text!dojonotes/Application.html",

    // Template Widgets
    "dijit/layout/BorderContainer",
    "dijit/layout/ContentPane",
    "dijit/form/Button",
    "dojonotes/views/widgets/Counter"

], function(declare, lang,
            _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin,
            dom, hash, topic,registry, on,
            NotesList, NoteUpdateDialog, NotesModel,
            template) {

    var notesModel = null;
    var notesListData = null;
    var notesList = null;
    var notesCounter = null;

    /**
     * @private
     * Initializes event listeners.
     */
    var initListeners = function() {
        var btnAddNote = registry.byId("btnAddNote");
        var btnDeleteSelectedNote = registry.byId("btnDeleteSelectedNote");
        if (btnAddNote) {
            btnAddNote.on("click", lang.hitch(this, onAddNoteButtonClick));
        }
        if (btnDeleteSelectedNote) {
            btnDeleteSelectedNote.on("click", lang.hitch(this, onDeleteSelectedNoteButtonClick));
        }
        topic.subscribe("/dojo/hashchange", lang.hitch(this, onHashChange));
    };

    /**
     * @private
     * Initializes model and UI.
     */
    var initModelAndUI = function() {
        notesModel = new NotesModel({});
        notesListData = notesModel.store.query();
        notesList = new NotesList(notesListData, dom.byId("notesList"));
        notesCounter = registry.byId("notesCounter");

        on(notesModel, "change", lang.hitch(this, onNotesModelUpdate));
        on(notesList, "selectedIdsChange", lang.hitch(this, onNotesListSelectedIdsChange));
        on(notesList, "itemClick", lang.hitch(this, onNotesListItemClick));

        onNotesListSelectedIdsChange(notesList);
        onNotesModelUpdate(notesModel);

        processHash(hash());
    };

    /**
     * Shows note update dialog, if noteItem === null, the dialog adds new note rather than updating
     * an existing one.
     *
     * @param noteItem Note item (optional), default is null.
     */
    var showNoteUpdateDialog = function(noteItem) {
        if (isNoteUpdateDialogOpen()) return;

        var noteUpdateDialog = new NoteUpdateDialog({ noteItem: noteItem });
        on(noteUpdateDialog, "save", lang.hitch(this, onNoteUpdateDialogSave));
        on(noteUpdateDialog, "close", lang.hitch(this, onNoteUpdateDialogClose));
        noteUpdateDialog.show();
        if (noteItem) {
            hash("/edit/" + noteItem.id);
        } else {
            hash("/new");
        }
    };

    /**
     * Closes note update dialog.
     */
    var closeNoteUpdateDialog = function() {
        var dialog = registry.byId("noteUpdateDialog");
        if (dialog !== undefined) {
            dialog.destroyRecursive();
        }
    };

    /**
     * Processes hash and trigger processes accordingly.
     *
     * @param hashTag Hash tag.
     */
    var processHash = function(hashTag) {
        var tags = hashTag.replace(/^\/+/, "").split("/");
        switch (tags[0]) {
            case "edit":
                var noteItem = notesModel.getOne(tags[1] * 1);
                if (noteItem) {
                    showNoteUpdateDialog(noteItem);
                }
                break;

            case "new":
                showNoteUpdateDialog();
                break;

            default:
                closeNoteUpdateDialog();
                break;
        }
    };

    /**
     * Returns true if note update dialog is open, false otherwise.
     *
     * @returns True or false.
     */
    var isNoteUpdateDialogOpen = function() {
        return registry.byId("noteUpdateDialog") !== undefined;
    };

    /**
     * @private
     * Handles when the number of items in a note list changes.
     *
     * @param target Dispatcher target.
     */
    var onNotesModelUpdate = function(target) {
        notesCounter.setValue(target.numItems);
    };

    /**
     * @private
     * Handles when a note checkbox is checked or unchecked.
     *
     * @param target Dispatcher target.
     */
    var onNotesListSelectedIdsChange = function(target) {
        registry.byId("btnDeleteSelectedNote").set("disabled", target.selectedIds.length === 0);
    };

    /**
     * @private
     * Handles when a note list item is clicked.
     *
     * @param target Dispatcher target.
     */
    var onNotesListItemClick = function(target) {
        var noteItem = notesModel.getOne(target.id);
        showNoteUpdateDialog(noteItem);
    };

    /**
     * @private
     * Handles event when note dialog is saved.
     */
    var onNoteUpdateDialogSave = function(target) {
        if (target.noteItem.id < 0) {
            notesModel.addOne(target.noteItem.title, target.noteItem.content);
        } else {
            notesModel.updateOne(target.noteItem.id, target.noteItem.title, target.noteItem.content);
        }
    };

    /**
     * @private
     * Handles event when note dialog is closed.
     */
    var onNoteUpdateDialogClose = function() {
        hash("");
    };

    /**
     * @private
     * Handles event when Add Note button is clicked.
     */
    var onAddNoteButtonClick = function() {
        showNoteUpdateDialog();
    };

    /**
     * @private
     * Handles event when Delete Selected Note button is clicked.
     */
    var onDeleteSelectedNoteButtonClick = function() {
        notesModel.removeMultiple(notesList.selectedIds);
    };

    /**
     * @private
     * Handles deep-linking.
     */
    var onHashChange = function(hashTag) {
        processHash(hashTag);
    };

    return declare("dojonotes.Application", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
        templateString: template,

        postCreate: function() {
            this.inherited(arguments);

            initListeners();
            initModelAndUI();
        }
    });
});