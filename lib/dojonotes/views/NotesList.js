/**
 * Contains list of notes.
 */
define([
    "dojo/_base/declare",
    "dojo/_base/lang",

    // Parent Classes
    "dijit/_WidgetBase",

    // General Modules
    "dojo/dom-construct",
    "dojo/on",

    // Custom Widgets
    "dojonotes/views/widgets/NoteListItem"

], function(declare, lang, _WidgetBase, domConstruct, on, NoteListItem) {
    var itemWidgets = [];
    var arSelectedIds = [];

    return declare("dojonotes.views.NotesList", [_WidgetBase], {
        _container: null,
        selectedIds: "",

        /**
         * Initializes notes list widget.
         *
         * @param model Model that the widget observes for changes.
         * @param container Widget container.
         */
        constructor: function(model, container) {
            if (!model) {
                throw Error("[NotesList ERROR] Model is not defined.");
            }
            if (!container) {
                throw Error("[NotesList ERROR] Container is not defined.");
            }

            this._container = container;
            model.forEach(lang.hitch(this, this.addNoteAt));
            model.observe(lang.hitch(this, this._onModelUpdate), true);
        },

        /**
         * Adds a note at a specified index.
         *
         * @param item Note item.
         * @param index Index position of the note.
         */
        addNoteAt: function(item, index) {
            var itemWidget = new NoteListItem(item);
            itemWidget.placeAt(this._container);
            itemWidgets.push(itemWidget);
            itemWidget.selectedChangeHandler = on(itemWidget, "selectedChange", lang.hitch(this, this._onCheckBoxChange));
            itemWidget.itemClickHandler = on(itemWidget, "itemClick", lang.hitch(this, this._onItemClick));
        },

        /**
         * Updates a note at a specified index.
         *
         * @param item Note item.
         * @param index Index position of the note.
         */
        updateNoteAt: function(item, index) {
            var itemWidget = itemWidgets[index];
            itemWidget.setTitle(item.title);
        },

        /**
         * Removes note at a specified index.
         *
         * @param index Index position of the note.
         */
        removeNoteAt: function(index) {
            var itemWidget = itemWidgets.splice(index, 1)[0];
            itemWidget.selectedChangeHandler.remove();
            itemWidget.itemClickHandler.remove();
            domConstruct.destroy("" + itemWidget.id);
        },

        /**
         * @private
         * Handles checkbox updates.
         *
         * @param target Dispatcher target.
         */
        _onCheckBoxChange: function(target) {
            var index = arSelectedIds.indexOf(target.id);
            // add
            if (target.selected && index === -1) {
                arSelectedIds.push(target.id);
            }
            // or delete
            else if (!target.selected && index !== -1) {
                arSelectedIds.splice(index, 1);
            }
            this.selectedIds = arSelectedIds.join(",");
            this.onSelectedIdsChange(this);
        },

        /**
         * @private
         * Handles item click.
         *
         * @param target Dispatcher target.
         */
        _onItemClick: function(target) {
            this.onItemClick(target);
        },

        /**
         * @private
         * Handles model updates.
         *
         * @param newItem Item to add, null if nothing to add.
         * @param removedIndex Index position for the item to remove, -1 if nothing to remove.
         * @param insertedIndex Index position for the item to add, -1 if nothing to add.
         */
        _onModelUpdate: function(newItem, removedIndex, insertedIndex) {
            if (insertedIndex > -1 && insertedIndex === removedIndex) {
                this.updateNoteAt(newItem, insertedIndex);
            } else if (removedIndex > -1 && insertedIndex === -1) {
                this.removeNoteAt(removedIndex);
            } else if (removedIndex === -1 && insertedIndex > -1) {
                this.addNoteAt(newItem, insertedIndex);
            }
        },

        /**
         * Dispatched when selected Ids value changes.
         *
         * @param target Dispatcher target.
         */
        onSelectedIdsChange: function(target) {
        },

        /**
         * Dispatched when a note item is clicked.
         *
         * @param target Dispatcher target.
         */
        onItemClick: function(target) {
        }
    });
});