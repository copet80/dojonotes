/**
 * Counter widget.
 */
define([
    "dojo/_base/declare",

    // Parent Classes
    "dijit/_WidgetBase"

], function(declare, _WidgetBase) {
    return declare("dojonotes.views.widgets.Counter", [_WidgetBase], {
        value: 0,
        baseClass: "counter",
        singular: "item",
        plural: "items",

        /**
         * @inheritDoc
         */
        postCreate: function() {
            this.inherited(arguments);
            this.setValue(0);
        },

        /**
         * Sets count value and selectively select singular and plural unit postfix.
         *
         * @param count Count value.
         */
        setValue: function(value) {
            this.value = value;
            var node = this.domNode;
            if (node) {
                node.innerHTML = value + ' ' + (value > 1 ? this.plural : this.singular);
            }
        }
    });
});