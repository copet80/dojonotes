/**
 * Label widget.
 */
define([
    "dojo/_base/declare",

    // Parent Classes
    "dijit/_WidgetBase"

], function(declare, _WidgetBase) {
    return declare("dojonotes.views.widgets.Label", [_WidgetBase], {
        baseClass: "label"
    });
});