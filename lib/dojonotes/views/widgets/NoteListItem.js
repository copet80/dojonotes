/**
 * Item renderer class for Note List.
 */
define([
    "dojo/_base/declare",
    "dojo/_base/lang",

    // Parent Classes
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",

    // General Modules
    "dijit/registry",
    "dojox/html/entities",

    // Widget Template
    "dojo/text!dojonotes/views/widgets/NoteListItem.html",

    // Template Widgets
    "dijit/form/CheckBox",
    "dojonotes/views/widgets/Label"

], function(declare, lang, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, registry, entities, template) {
    return declare("dojonotes.views.widgets.NoteListItem", [_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {
        _checkbox: null,
        _label: null,

        templateString: template,
        id: 0,
        title: "",
        baseClass: "noteListItem",
        selected: false,

        /**
         * @inherit
         */
        postCreate: function() {
            this.inherited(arguments);
            this._checkBox = registry.byId("noteCheckBox" + this.id);
            this._checkBox.on("change", lang.hitch(this, this._onCheckBoxChange));
            this._label = registry.byId("noteTitleLabel" + this.id);
            this._label.on("click", lang.hitch(this, this._onLabelClick));
            this.setTitle(this.title);
        },

        /**
         * Sets title label.
         *
         * @param value Label text.
         */
        setTitle: function(value) {
            this.title = value;
            this._label.domNode.innerHTML = entities.encode(this.title);
        },

        /**
         * @private
         * Handles checkbox widget check and unchecked event.
         */
        _onCheckBoxChange: function() {
            this.selected = this._checkBox.checked;
            this.onSelectedChange(this);
        },

        /**
         * @private
         * Triggered when the note label is clicked.
         */
        _onLabelClick: function() {
            this.onItemClick(this);
        },

        /**
         * Dispatched when checkbox is checked and unchecked.
         */
        onSelectedChange: function(target) {
        },

        /**
         * Dispatched when a note item is clicked.
         */
        onItemClick: function(target) {
        }
    });
});