/**
 * Contains note update form in a dialog.
 */
define([
    "dojo/_base/declare",
    "dojo/_base/lang",

    // Parent Classes
    "dijit/Dialog",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",

    // General Modules
    "dijit/registry",

    // Widget Template
    "dojo/text!dojonotes/views/NoteUpdateDialog.html",

    // Template Widgets
    "dijit/form/TextBox",
    "dijit/form/Button",
    "dijit/Editor"

], function(declare, lang, Dialog, _TemplatedMixin, _WidgetsInTemplateMixin, registry, template) {
    return declare("dojonotes.views.NoteUpdateDialog", [Dialog, _TemplatedMixin, _WidgetsInTemplateMixin], {
        _titleInput: null,
        _contentEditor: null,
        _btnSave: null,
        _btnCancel: null,

        id: "noteUpdateDialog",
        title: "Update Note",
        noteItem: null,
        content: template,
        style: "min-width: 400px; width: 70%; height:405px;",

        postCreate: function() {
            this.inherited(arguments);

            this._titleInput = registry.byId("titleInput");
            this._titleInput.intermediateChanges = true;
            this._titleInput.on("change", lang.hitch(this, this._onTitleInputChange));

            this._contentEditor = registry.byId("contentEditor");

            this._btnSave = registry.byId("btnSave");
            this._btnSave.on("click", lang.hitch(this, this._onSaveButtonClick));
            this._btnSave.set("disabled", true);

            this._btnCancel = registry.byId("btnCancel");
            this._btnCancel.on("click", lang.hitch(this, this._onCancelButtonClick));

            this.noteItem = this.noteItem || { id: -1, title: "", content: ""};
            this._titleInput.attr("value", this.noteItem.title);
            this._contentEditor.attr("value", this.noteItem.content);

            this.attr("title", (this.noteItem ? "Update Note" : "Add Note"));
        },

        /**
         * @private
         * Handles title input changes (intermediate).
         */
        _onTitleInputChange: function() {
            this._btnSave.set("disabled", this._titleInput.value.length === 0);
        },

        /**
         * @private
         * Handles save button click.
         */
        _onSaveButtonClick: function() {
            this.noteItem.title = this._titleInput.value;
            this.noteItem.content = this._contentEditor.value;
            this.onSave(this);
            this.destroyRecursive();
        },

        /**
         * @private
         * Handles cancel button click.
         */
        _onCancelButtonClick: function() {
            this.destroyRecursive();
        },

        /**
         * @inheritDoc
         */
        hide: function() {
            this.inherited(arguments);
            this.destroyRecursive();
        },

        /**
         * @inheritDoc
         */
        destroyRecursive: function() {
            this.onClose(this);
            this._btnSave.destroyRecursive();
            this._btnCancel.destroyRecursive();
            this.inherited(arguments);
        },

        /**
         * Dispatched when a note item is clicked.
         *
         * @param target Dispatcher target.
         */
        onSave: function(target) {
        },

        /**
         * Dispatched when destroy recursive is called.
         *
         * @param target Dispatcher target.
         */
        onClose: function(target) {
        }
    });
});